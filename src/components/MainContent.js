import React from "react"
import Chat from "./chat/Chat";


class MainContent extends React.Component {

    render() {
        return (
            <div className="main-content">
                <Chat/>
            </div>

        )
    }

}

export default MainContent
