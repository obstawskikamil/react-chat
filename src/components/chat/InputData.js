import React from "react"
import TextField from "@material-ui/core/TextField";


class InputData extends React.Component {

    render() {
        return (

            <form>
                <TextField
                    style={{width: "95%", marginTop: "1%"}}
                    name="inputData"
                    placeholder="Chat here ..."
                    value={this.props.inputData}
                    onChange={this.props.onChange}
                    autoComplete="off"
                    onKeyDown={this.props.onKeyDown}
                />
            </form>

        )
    }
}


export default InputData
