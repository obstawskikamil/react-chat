import React from "react"


class Message extends React.Component {

    constructor(props) {
        super(props)
        this.copyMessage = this.copyMessage.bind(this)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.message === this.props.message) {
            return false
        }
        return true
    }

    copyMessage() {
        return navigator.clipboard.writeText(this.props.message.content)
    }

    render() {
        return (

            <div className="msg right-msg">
                <div
                    className="msg-img"
                    style={{backgroundImage: "url('https://avatarfiles.alphacoders.com/175/175840.jpg')"}}
                >
                </div>

                <div className="msg-bubble">
                    <div className="msg-info">
                        <div className="msg-info-name">{this.props.message.added_by}</div>
                        <div className="msg-info-time">{this.props.message.added_at}</div>
                    </div>

                    <div className="msg-text">
                        {this.props.message.content}
                    </div>
                </div>

                <div className="hidden-panel">
                    <button className="feature-button copy-button" onClick={this.copyMessage}>copy</button>
                    <button className="feature-button delete-button" onClick={() => this.props.deleteMessage(this.props.message.added_at)}>delete</button>
                </div>

            </div>

        )
    }
}


export default Message
