import React from 'react';
import ChatContent from "./ChatContent";


class Chat extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            inputData: "",
            message: {
                added_at: "",
                added_by: "",
                content: "",
            },
            messages: [],
        }

        this.onChange = this.onChange.bind(this)
        this.onKeyDown = this.onKeyDown.bind(this)
        this.scrollToBottom = this.scrollToBottom.bind(this)
        this.addMessage = this.addMessage.bind(this)
        this.clearInputData = this.clearInputData.bind(this)
        this.deleteMessage = this.deleteMessage.bind(this)
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    addMessage() {
        this.setState(prevState => {
            return {
                messages: [...prevState.messages, prevState.message]
            }
        })
    }

    clearInputData() {
        this.setState({inputData: ""})
    }

    deleteMessage(key) {
        this.setState(prevState => {
            return {
                messages: prevState.messages.filter(function(message) {
                    return message.added_at !== key
                })
            }
        });
    }

    formatTime(date) {
        const hours = date.getHours()
        const minutes = date.getMinutes()
        const seconds = date.getSeconds()
        const milliseconds = date.getMilliseconds()

        return hours + ':' + minutes + ':' + seconds + ':' + milliseconds
    }

    onChange(event) {
        const {name, value} = event.target

        this.setState({[name]: value})
    }

    onKeyDown(event) {
        if (event.key === 'Enter') {
            event.preventDefault()
            this.send()
        }
    }

    scrollToBottom() {
        const chat = document.getElementById('msger-chat')
        chat.scrollTop += 500
    }

    send() {
        this.setState(prevState => {
            return {
                message: {
                    added_at: this.formatTime(new Date()),
                    added_by: "Sergio Marquina",
                    content: prevState.inputData,
                }
            }
        })

        this.clearInputData()
        this.addMessage()
    }

    render() {

        return (
            <ChatContent
                data={this.state}
                deleteMessage={this.deleteMessage}
                onChange={this.onChange}
                onKeyDown={this.onKeyDown}
                formatTime={this.formatTime}
            />
        );
    }

}

export default Chat
