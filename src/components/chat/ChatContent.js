import React from 'react';
import Paper from '@material-ui/core/Paper';
import Message from "./Message";
import InputData from "./InputData";


const date = new Date()


class ChatContent extends React.Component {

    render() {

        return (

            <div>
                <Paper className="paper">
                    <h3 className="chat-header">
                        Chat here with Your bot friend!
                    </h3>
                    <section className="msger">
                        <main className="msger-chat" id="msger-chat">

                            <div className="msg left-msg">
                                <div
                                    className="msg-img"
                                    style={{backgroundImage: "url('https://image.flaticon.com/icons/svg/327/327779.svg')"}}
                                >

                                </div>

                                <div className="msg-bubble">
                                    <div className="msg-info">
                                        <div className="msg-info-name">BOT</div>
                                        <div className="msg-info-time">
                                            {this.props.formatTime(date)}
                                        </div>
                                    </div>

                                    <div className="msg-text">
                                        Hi, welcome to SimpleChat! Go ahead and send me
                                        a message. 😄
                                    </div>
                                </div>
                            </div>

                            {this.props.data.messages.map(message => {
                                return (
                                    <Message
                                        key={message.added_at}
                                        message={message}
                                        deleteMessage={this.props.deleteMessage}
                                    />
                                )
                            })}

                        </main>
                    </section>

                    <InputData
                        inputData={this.props.data.inputData}
                        onChange={this.props.onChange}
                        onKeyDown={this.props.onKeyDown}
                    />
                </Paper>
            </div>
        );
    }


}

export default ChatContent
